<?php

namespace EBANX\Account;

class Security extends Access
{
	protected function balance( array $id )
	{
		if ( $_SERVER[ 'REQUEST_METHOD' ] != 'GET' )
			$this -> sendOutput( NULL, [ 'HTTP/1.1 404 Not Found' ] );

		parent :: balance( $id );

	}

	protected function event( )
	{
		if ( $_SERVER[ 'REQUEST_METHOD' ] != 'POST' )
			$this -> sendOutput( NULL, [ 'HTTP/1.1 404 Not Found' ] );

		parent :: event( );
	}

	protected function reset( )
	{
		if ( $_SERVER[ 'REQUEST_METHOD' ] != 'POST' )
			$this -> sendOutput( NULL, [ 'HTTP/1.1 404 Not Found' ] );

		parent :: reset( );
	}
}

?>
