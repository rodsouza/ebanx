<?php

namespace EBANX\Account;

class Controller
{
	/**
	 * __construct magic method.
	 */
	protected function __construct( )
	{
		if ( count( $action = $this -> getUriSegments( ) ) )
			$this -> {$action[ 0 ]}(
				$this -> getQueryStringParams( )
			);
	}

	/**
	 * __call magic method.
	 */
	public function __call( $name, $arguments )
	{
		$this -> sendOutput( NULL, [ 'HTTP/1.1 404 Not Found' ] );
	}

	/**
	 * Get URI elements.
	 * 
	 * @return array
	 */
	protected function getUriSegments( ) : array
	{
		$uri = parse_url( $_SERVER[ 'REQUEST_URI' ], PHP_URL_PATH );
		$uri = strlen( $uri ) > 1 ? substr( $uri, 1 ) : NULL;
		return $uri ? explode( '/', $uri ) : [ ];
	}

	/**
	 * Get querystring params.
	 * 
	 * @return array
	 */
	protected function getQueryStringParams( ) : array
	{
		parse_str($_SERVER['QUERY_STRING'], $query);
		return $query;
	}

	/**
	 * Send API output.
	 *
	 * @param mixed  $data
	 * @param string $headers
	 */
	protected function sendOutput( string $data = NULL, array $headers = [ ] )
	{
		header_remove( 'Set-Cookie' );

		foreach ( $headers as $header )
			header( $header );

		echo $data;
		exit;
	}
}
