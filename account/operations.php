<?php

namespace EBANX\Account;

class Operations extends Controller
{
	protected function deposit( string $id = NULL, int $amount = NULL ) : array
	{
		if ( ! array_key_exists( $id, $this -> accounts ) )
			$this -> accounts[ $id ] = [ 'balance' => 0 ];

		$this -> accounts[ $id ][ 'balance' ] += $amount;

		return [
			'destination' => [
				'id' => $id,
				'balance' => $this -> accounts[ $id ][ 'balance' ]
			]
		];
	}

	protected function transfer ( string $orig = NULL, int $amount = NULL, string $dest ) : array
	{
		if ( ! array_key_exists( $orig, $this -> accounts ) )
			$this -> sendOutput( 0, [ 'HTTP/1.1 404 Not Found' ] );

		return array_merge(
			$this -> withdraw( $orig, $amount ),
			$this -> deposit( $dest, $amount )
		);
	}

	protected function withdraw ( string $id = NULL, int $amount = NULL ) : array
	{
		if ( ! array_key_exists( $id, $this -> accounts ) )
			$this -> sendOutput( 0, [ 'HTTP/1.1 404 Not Found' ] );

		$this -> accounts[ $id ][ 'balance' ] -= $amount;


		return [
			'origin' => [
				'id' => $id,
				'balance' => $this -> accounts[ $id ][ 'balance' ]
			]
		];
	}
}

?>
