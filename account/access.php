<?php

namespace EBANX\Account;

class Access extends Operations
{
	protected $accounts;

	public function __construct( )
	{
		$this -> accounts = json_decode(
			file_get_contents( 'accounts.json' ),
			flags: JSON_OBJECT_AS_ARRAY
		);

		register_shutdown_function( [ $this, '__destruct' ] );

		parent :: __construct( );
	}

	public function __destruct( )
	{
		file_put_contents( 'accounts.json', json_encode( $this -> accounts ) );
	}

	protected function balance( array $id )
	{
		$id = array_key_exists( 'account_id', $id ) ? $id[ 'account_id' ] : NULL;

		if ( ! array_key_existS( $id, $this -> accounts ) )
			$this -> sendOutput( 0, [ 'HTTP/1.1 404 Not Found' ] );

		$this -> sendOutput( $this -> accounts[ $id ][ 'balance' ], [ 'HTTP/1.1 200' ] );
	}

	protected function event( )
	{
		$event = json_decode( file_get_contents( 'php://input' ) );

		switch ( $event -> type )
		{
			case 'deposit' :
			if ( $event -> destination && $event -> amount )
				break;
				
			case 'transfer' :
			if ( $event -> origin && $event -> amount && $event -> destination )
				break;

			case 'withdraw' :
			if ( $event -> origin && $event -> amount )
				break;

			default :
				$this -> sendOutput( NULL, [ 'HTTP/1.1 404 Not Found' ] );
		}

		$out = $this -> {$event -> type}(
			$event -> origin ??
				( $event -> destination ?? NULL ),
			$event -> amount,
			$event -> destination
		);

		$this -> sendOutput( json_encode( $out ), [ 'HTTP/1.1 201' ] );
	}

	protected function reset( )
	{
		$this -> accounts = [ ];
		$this -> sendOutput( 'OK', [ 'HTTP/1.1 200' ] );
	}
}

?>
