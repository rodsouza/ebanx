<?php

spl_autoload_register(function ($name) {
	$name = substr( $name, 6 );
	$name = str_replace( '\\', '/', $name);
	require_once strtolower( "{$name}.php" );
});

new \EBANX\Account\Security;

?>
